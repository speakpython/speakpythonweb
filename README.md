# Welcome to SpeakPython

SpeakPython is the one-stop forum for Python geeks all around the world!.

## How to start development
### Step 1: Install pip

    $ sudo apt-get install python-pip


### Step 2: Install virtualenv

    $ pip install virtualenv


### Step 3: Create new virtualenv for SpeakPython

    ~/home$ mkdir virtualenvs
    ~/home$ cd virtualenvs
    ~/home/virtualenvs$ virtualenv speakpython


### Step 4: Activate the new virtualenv

    ~/home/virtualenvs$ source speakpython/bin/activate

Then you will notice the virtualenv is activated. Look for the virtualenv name within brackets on shell.

example:

    (speakpython)user@machine:~/home


### Step 5: Installing dependencies
With the speakpython virtualenv activated, navigate into the git repo directory and type the following:

    ~/home/speakpython$ pip install -r requirements.txt

This will install all python dependencies to the currently activated virtualenv.


### Step 6: Edit settings_local.py

Create a new database on mysql and update the settings_local.py with the database details

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'osqa',
            'USER': 'root',
            'PASSWORD': 'user@123',
            'HOST': '',
            'PORT': '',
        }
    }


### Step 7: Initialize database

Now you can initialize the database by typing the following:

    ~/home/speakpython$ python manage.py syncdb --all
    ~/home/speakpython$ python manage.py migrate forum --fake


### Step 8: All done! Now run the development server

    ~/home/speakpython$ python manage.py runserver

Hack away! :D